package com.jstk.movie.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.jstk.movie.domain.CastEntity;
import com.jstk.movie.types.CastTO;

@Mapper(componentModel="spring")
public interface CastMapper {
	CastMapper INSTANCE = Mappers.getMapper(CastMapper.class);

	CastTO castEntity2castTo(CastEntity castEntity);

	CastEntity castTo2castEntity(CastTO castTo);
}
