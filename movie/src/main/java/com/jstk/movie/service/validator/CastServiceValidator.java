package com.jstk.movie.service.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CastServiceValidator {
	@Value("${max.movie.per.period}")
    private int maxNoOfFilmsPerPeriod;
}
