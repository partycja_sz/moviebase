package com.jstk.movie.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.jstk.movie.domain.MoneyEntity;
import com.jstk.movie.types.MoneyTO;

@Mapper(componentModel="spring")
public interface MoneyMapper {
	MoneyMapper INSTANCE = Mappers.getMapper(MoneyMapper.class);

	MoneyTO moneyEntity2moneyTo(MoneyEntity moneyEntity);

	MoneyEntity moneyTo2moneyEntity(MoneyTO moneyTo);
}
