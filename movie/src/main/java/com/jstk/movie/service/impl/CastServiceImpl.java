package com.jstk.movie.service.impl;

import java.util.NoSuchElementException;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jstk.movie.dao.CastRepository;
import com.jstk.movie.domain.CastEntity;
import com.jstk.movie.mappers.CastMapper;
import com.jstk.movie.service.CastService;
import com.jstk.movie.types.CastTO;
@Service
@Transactional
public class CastServiceImpl implements CastService {

	@Autowired
	private CastRepository castRepository;
	
	@Autowired
	private CastMapper castMapper;
	
	@Override
	public CastTO addCast(CastTO castTo) {
		CastEntity castEntity = castRepository.save(castMapper.castTo2castEntity(castTo));
		return castMapper.castEntity2castTo(castEntity);
	}

	@Override
	public CastTO findCastById(Long id) {
		Optional<CastEntity> castEntity = castRepository.findById(id);
		if (castEntity.isPresent()){
			return castMapper.castEntity2castTo(castEntity.get());
		}
		else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public CastTO updateCast(CastTO castTo) {
		CastEntity castEntity = castRepository.saveAndFlush(castMapper.castTo2castEntity(castTo));
		return castMapper.castEntity2castTo(castEntity);
	}

}
