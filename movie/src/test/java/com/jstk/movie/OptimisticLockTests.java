package com.jstk.movie;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import com.jstk.movie.enums.MovieCategory;
import com.jstk.movie.enums.MovieType;
import com.jstk.movie.exceptions.DuplicatedMovieHasBeenFoundException;
import com.jstk.movie.service.CastService;
import com.jstk.movie.service.MovieService;
import com.jstk.movie.service.StudioService;
import com.jstk.movie.types.CastTO;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.StudioTO;
import com.jstk.movie.types.CastTO.CastTOBuilder;
import com.jstk.movie.types.MovieTO.MovieTOBuilder;
import com.jstk.movie.types.StudioTO.StudioTOBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OptimisticLockTests {

	@Autowired
	private StudioService studioService;

	@Autowired
	private MovieService movieService;

	@Autowired
	private CastService castService;

	DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Test(expected = ObjectOptimisticLockingFailureException.class)
	public void checkOtimistickLockExceptionStudioTest() {
		// given
		String name = "Digital";
		String name2 = "Digital ";
		String address = "Cinema Street 12345, Los Angeles, USA";
		String addressUpdated = "New Street great 453/23, Los Angeles, USA";
		StudioTO studioTo = new StudioTOBuilder().withName(name).withAddress(address).build();
		StudioTO addedStudio = studioService.addStudio(studioTo);

		StudioTO entityStudio = studioService.findStudioById(addedStudio.getId());
		StudioTO entityStudio2 = studioService.findStudioById(addedStudio.getId());

		// when&then
		entityStudio.setAddress(addressUpdated);
		studioService.updateStudio(entityStudio);
		entityStudio2.setName(name2);
		studioService.updateStudio(entityStudio2);
	}

	@Test(expected = ObjectOptimisticLockingFailureException.class)
	public void checkOtimistickLockExceptionCastTest() {
		// given
		String dateRelaseString = "2003-05-15";
		String endRelaseString = "2013-05-15";
		LocalDate startCooperation = LocalDate.parse(dateRelaseString);
		LocalDate endCooperation = LocalDate.parse(endRelaseString);

		String firstName = "Adam";
		String lastName = "Ray";

		CastTO castTo = new CastTOBuilder().withFirstName(firstName).withLastName(lastName)
				.withStartCooperationDate(startCooperation).withEndCooperationDate(endCooperation).build();
		CastTO addedCast = castService.addCast(castTo);

		CastTO selectedCast1 = castService.findCastById(addedCast.getId());
		CastTO selectedCast2 = castService.findCastById(addedCast.getId());

		// when&then
		selectedCast1.setLastName("new name");
		selectedCast2.setFirstName("John");

		castService.updateCast(selectedCast1);
		castService.updateCast(selectedCast2);
	}

	@Test(expected = ObjectOptimisticLockingFailureException.class)
	public void checkOtimisticLockExceptionTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";

		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);

		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		MovieTO movieOne = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();

		MovieTO addedMovieOne = movieService.addMovie(movieOne);

		MovieTO selectedMovie1 = movieService.findMovieById(addedMovieOne.getId());
		MovieTO selectedMovie2 = movieService.findMovieById(addedMovieOne.getId());

		// when&then
		selectedMovie1.setCountry("Portugal");
		selectedMovie2.setRuntime(115);

		movieService.updateMovie(selectedMovie1);
		movieService.updateMovie(selectedMovie2);
	}

}
