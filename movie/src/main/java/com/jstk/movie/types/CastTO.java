package com.jstk.movie.types;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class CastTO {

	private Long id;
	private String firstName;
	private String lastName;
	private List<StudioTO> studioTOs;
	private List<MovieTO> movieTOs;
	private LocalDate startCooperation;
	private LocalDate endCooperation;
	private Long version;
	private LocalDateTime  creationDate;
	private LocalDateTime  modyficationDate;

	public CastTO(Long id, String firstName, String lastName, List<StudioTO> studioTO, LocalDate startCooperation,
			LocalDate endCooperation) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.studioTOs = studioTO;
		this.startCooperation = startCooperation;
		this.endCooperation = endCooperation;
	}

	public CastTO(Long id, String firstName, String lastName, List<StudioTO> studioTOs, List<MovieTO> movieTOs,
			LocalDate startCooperation, LocalDate endCooperation, Long version, LocalDateTime  creationDate, LocalDateTime  modyficationDate) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.studioTOs = studioTOs;
		this.movieTOs = movieTOs;
		this.startCooperation = startCooperation;
		this.endCooperation = endCooperation;
		this.version = version;
		this.creationDate = creationDate;
		this.modyficationDate = modyficationDate;
	}

	public CastTO() {
	}

	public Long getId() {
		return id;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public void setCreationDate(LocalDateTime  creationDate) {
		this.creationDate = creationDate;
	}

	public void setModyficationDate(LocalDateTime  modyficationDate) {
		this.modyficationDate = modyficationDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<StudioTO> getStudioTO() {
		return studioTOs;
	}

	public void setStudioTO(List<StudioTO> studioTO) {
		this.studioTOs = studioTO;
	}

	public LocalDate getStartCooperation() {
		return startCooperation;
	}

	public void setStartCooperation(LocalDate startCooperation) {
		this.startCooperation = startCooperation;
	}

	public LocalDate getEndCooperation() {
		return endCooperation;
	}

	public void setEndCooperation(LocalDate endCooperation) {
		this.endCooperation = endCooperation;
	}

	public static CastTOBuilder builder() {
		return new CastTOBuilder();
	}

	public List<StudioTO> getStudioTOs() {
		return studioTOs;
	}

	public void setStudioTOs(List<StudioTO> studioTOs) {
		this.studioTOs = studioTOs;
	}

	public List<MovieTO> getMovieTOs() {
		return movieTOs;
	}

	public void setMovieTOs(List<MovieTO> movieTOs) {
		this.movieTOs = movieTOs;
	}

	public Long getVersion() {
		return version;
	}

	public LocalDateTime  getCreationDate() {
		return creationDate;
	}

	public LocalDateTime  getModyficationDate() {
		return modyficationDate;
	}

	public static class CastTOBuilder {
		private Long id;
		private String firstName;
		private String lastName;
		private List<StudioTO> studioTO;
		private LocalDate startCooperation;
		private LocalDate endCooperation;

		public CastTOBuilder() {
			super();
		}

		public CastTOBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public CastTOBuilder withFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public CastTOBuilder withLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public CastTOBuilder withStartCooperationDate(LocalDate startCooperation) {
			this.startCooperation = startCooperation;
			return this;
		}

		public CastTOBuilder withEndCooperationDate(LocalDate endCooperation) {
			this.endCooperation = endCooperation;
			return this;
		}

		public CastTOBuilder withStudioTO(List<StudioTO> studioTO) {
			this.studioTO = studioTO;
			return this;
		}

		public CastTO build() {
			checkBeforeBuild(firstName, lastName, studioTO, startCooperation, endCooperation);
			return new CastTO(id, firstName, lastName, studioTO, startCooperation, endCooperation);
		}

		private void checkBeforeBuild(String firstName, String lastName, List<StudioTO> studioTO, LocalDate startCooperation,
				LocalDate endCooperation) {
			if (firstName == null || firstName.isEmpty() || lastName == null || lastName.isEmpty()) {
				throw new IllegalArgumentException("Incorrect cast to be created");
			}
		}
	}
}
