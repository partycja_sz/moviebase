package com.jstk.movie.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.jstk.movie.domain.MovieEntity;
import com.jstk.movie.types.MovieTO;

@Mapper(componentModel="spring")
public interface MovieMapper {
	MovieMapper INSTANCE = Mappers.getMapper(MovieMapper.class);

	MovieTO movieEntity2movieTo(MovieEntity movieEntity);

	MovieEntity movieTo2movieEntity(MovieTO movieTo);
}
