package com.jstk.movie.custom.dao;

import java.util.Map;

import com.jstk.movie.domain.StudioEntity;

public interface StudioRepositoryCustom {
	Map<StudioEntity, Long> findAmountMoviePerStudioInSpecifiedYear(int year);
}
