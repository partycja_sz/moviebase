package com.jstk.movie.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.jstk.movie.criteria.MovieSearchCriteria;
import com.jstk.movie.domain.MovieEntity;
import com.jstk.movie.exceptions.DuplicatedMovieHasBeenFoundException;
import com.jstk.movie.types.MoneyTO;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.StudioTO;

public interface MovieService {

	MovieTO addMovie(MovieTO movieTo) throws DuplicatedMovieHasBeenFoundException;

	MovieTO updateMovie(MovieTO addedMovie);

	MovieTO findMovieById(Long id);

	List<MovieEntity> findTheLongestMovieInSpecifiedStudioInSpecifiedTime(LocalDate dateFrom, LocalDate dateTo,
			StudioTO studioTo);

	List<MovieEntity> findAllMoviesByCriteria(MovieSearchCriteria movieSearchCriteria);

	List<BigDecimal> findMoviesBudgetBetweenSpecifiedDates(int dateFrom, int dateTo);
	
	MovieTO addMoneyWeekToMovie(MovieTO addedMovie, MoneyTO moneyTo);

	Map<MovieEntity, BigDecimal> findWholeEarnMoneyForSpecifiedNoOfMovies(int noOfTopExpensiveMovies);

	void deleteMovieById(Long id); 
	
	Long countMovies(); 
	
	Long countMoneys(); 
}
