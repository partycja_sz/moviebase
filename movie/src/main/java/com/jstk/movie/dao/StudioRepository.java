package com.jstk.movie.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jstk.movie.custom.dao.StudioRepositoryCustom;
import com.jstk.movie.domain.StudioEntity;

public interface StudioRepository extends JpaRepository<StudioEntity, Long> , StudioRepositoryCustom{


}
