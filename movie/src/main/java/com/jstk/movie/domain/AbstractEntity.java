package com.jstk.movie.domain;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

@MappedSuperclass
public abstract class AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;
	@Column(name = "creation_date")
	protected LocalDateTime  creationDate;
	@Column(name = "modify_date")
	protected LocalDateTime  modyficationDate;
	@Version
	protected Long version;

	@PrePersist
	public void prePersist() {
		setCreationDate(LocalDateTime.now());
	}

	@PreUpdate
	public void preUpdate() {
		setModyficationDate(LocalDateTime.now());
		setVersion(this.getVersion()+1L);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getModyficationDate() {
		return modyficationDate;
	}

	public void setModyficationDate(LocalDateTime modyficationDate) {
		this.modyficationDate = modyficationDate;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}
