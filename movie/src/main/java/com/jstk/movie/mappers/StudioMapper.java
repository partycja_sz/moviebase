package com.jstk.movie.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.jstk.movie.domain.StudioEntity;
import com.jstk.movie.types.StudioTO;

@Mapper(componentModel="spring")
public interface StudioMapper {
	
	StudioMapper INSTANCE = Mappers.getMapper(StudioMapper.class);

	StudioTO studioEntity2studioTo(StudioEntity studioEntity);

    StudioEntity studioTo2studioEntity(StudioTO studioTo);
}
