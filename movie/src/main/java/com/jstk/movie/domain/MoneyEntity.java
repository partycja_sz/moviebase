package com.jstk.movie.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class MoneyEntity extends AbstractEntity {

	@Column(name = "profit_week", precision = 10, scale = 2)
	private BigDecimal weekProfit;

	@Column(name = "week_no")
	private int noOfWeek;

	public BigDecimal getWeekProfit() {
		return weekProfit;
	}

	public void setWeekProfit(BigDecimal weekProfit) {
		this.weekProfit = weekProfit;
	}

	public int getNoOfWeek() {
		return noOfWeek;
	}

	public void setNoOfWeek(int noOfWeek) {
		this.noOfWeek = noOfWeek;
	}

	public MoneyEntity(BigDecimal weekProfit, int noOfWeek) {
		super();
		this.weekProfit = weekProfit;
		this.noOfWeek = noOfWeek;
	}

	public MoneyEntity() {
	}
}
