package com.jstk.movie.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class CastEntity extends AbstractEntity{
	
	@Column(name = "first_name", length = 50, nullable = false)
	private String firstName;
	@Column(name = "last_name", length = 50, nullable = false)
	private String lastName;

	@Column(name = "start_date_cooperation")
	private LocalDate startCooperation;
	@Column(name = "end_date_cooperation")
	private LocalDate endCooperation;
	
	@ManyToMany(mappedBy = "casts")
	private List<StudioEntity> studioEntity = new ArrayList<>();
	
	@ManyToMany(mappedBy= "casts")
	private List<MovieEntity> movies = new ArrayList<>();
	
	public CastEntity(){}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<StudioEntity> getStudioEntity() {
		return studioEntity;
	}

	public void setStudioEntity(List<StudioEntity> studioEntity) {
		this.studioEntity = studioEntity;
	}

	public LocalDate getStartCooperation() {
		return startCooperation;
	}

	public void setStartCooperation(LocalDate startCooperation) {
		this.startCooperation = startCooperation;
	}

	public LocalDate getEndCooperation() {
		return endCooperation;
	}

	public void setEndCooperation(LocalDate endCooperation) {
		this.endCooperation = endCooperation;
	}

	public List<MovieEntity> getMovies() {
		return movies;
	}

	public void setMovies(List<MovieEntity> movies) {
		this.movies = movies;
	}

	public CastEntity(String firstName, String lastName, LocalDate startCooperation,
			LocalDate endCooperation) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.startCooperation = startCooperation;
		this.endCooperation = endCooperation;
	}
	
}
