package com.jstk.movie.types;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.jstk.movie.domain.MovieEntity;

public class StudioTO {
	private Long id;
	private String name;
	private String address;
	private Long version;
	private LocalDateTime  creationDate;
	private LocalDateTime  modyficationDate;
	private List<MovieEntity> movies = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getModyficationDate() {
		return modyficationDate;
	}

	public void setModyficationDate(LocalDateTime modyficationDate) {
		this.modyficationDate = modyficationDate;
	}

	public List<MovieEntity> getMovies() {
		return movies;
	}

	public void setMovies(List<MovieEntity> movies) {
		this.movies = movies;
	}

	public StudioTO(Long id, String name, String address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}
	
	public StudioTO(Long id, String name, String address, Long version, LocalDateTime  creationDate, LocalDateTime  modyficationDate, List<MovieEntity> movies) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.version = version;
		this.creationDate = creationDate;
		this.modyficationDate = modyficationDate;
		this.movies = movies;
	}

	public StudioTO(){}
	
	public static StudioTOBuilder builder() {
		return new StudioTOBuilder();
	}
	
	public static class StudioTOBuilder {
		private Long id;
		private String name;
		private String address;

		public StudioTOBuilder() {
			super();
		}

		public StudioTOBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public StudioTOBuilder withName(String name) {
			this.name = name;
			return this;
		}

		public StudioTOBuilder withAddress(String address) {
			this.address = address;
			return this;
		}
		
		public StudioTO build() {
			checkBeforeBuild( name,  address );
			return new StudioTO(id,  name,  address );
		}

		private void checkBeforeBuild(String name, String address) {
			if (name == null || name.isEmpty() || address == null || address.isEmpty() ) {
				throw new IllegalArgumentException("Incorrect studio to be created");
			}
		}
	}
}
