package com.jstk.movie.service;

import java.util.Map;

import com.jstk.movie.domain.StudioEntity;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.StudioTO;

public interface StudioService {
	StudioTO addStudio(StudioTO studioTo);

	void removeStudio(StudioTO addedStudio);

	long countAllStudios();

	StudioTO updateStudio(StudioTO studioTo);

	StudioTO findStudioById(Long id);

	Map<StudioEntity, Long> findAmountMoviePerStudioInSpecifiedYear(int year);

	void addMovieToStudio(StudioTO addedStudio, MovieTO addedMovie);

}
