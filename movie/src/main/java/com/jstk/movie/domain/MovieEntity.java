package com.jstk.movie.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import com.jstk.movie.enums.MovieCategory;
import com.jstk.movie.enums.MovieType;

@Entity
public class MovieEntity extends AbstractEntity {

	@Column(length = 50, nullable = false)
	private String title;
	@Column(name = "movie_category", nullable = false)
	@Enumerated(EnumType.STRING)
	private MovieCategory movieCategory;
	@Column(name = "movie_type", nullable = false)
	@Enumerated(EnumType.STRING)
	private MovieType movieType;
	@Column(name = "movie_length", nullable = false)
	private int runtime;
	@Column(name = "release_date", nullable = false)
	private LocalDate releaseDate;

	@Column(length = 100, nullable = false)
	private String country;

	@Column(name = "is_3DMovie", nullable = false)
	private Boolean threeD;
	@Column(precision = 10, scale = 2)
	private BigDecimal budget;
	@Column(name = "profit_general", precision = 10, scale = 2)
	private BigDecimal profitGeneral;
	@Column(name = "profit_one_week", precision = 10, scale = 2)
	private BigDecimal oneWeekProfit;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinTable(name = "movie_cast", joinColumns = { @JoinColumn(name = "movie_id") }, inverseJoinColumns = {
			@JoinColumn(name = "cast_id") })
	private List<CastEntity> casts = new ArrayList<>();

	@ManyToOne
	private StudioEntity studio;

	@OneToMany(cascade = { CascadeType.PERSIST }, 
			fetch = FetchType.LAZY, orphanRemoval=true)
	@JoinColumn(name = "movie_id")
	private List<MoneyEntity> money;
	
	public MovieEntity() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MovieCategory getMovieCategory() {
		return movieCategory;
	}

	public void setMovieCategory(MovieCategory movieCategory) {
		this.movieCategory = movieCategory;
	}

	public MovieType getMovieType() {
		return movieType;
	}

	public void setMovieType(MovieType movieType) {
		this.movieType = movieType;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public LocalDate getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}

	public StudioEntity getStudio() {
		return studio;
	}

	public void setStudio(StudioEntity studio) {
		this.studio = studio;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Boolean getThreeD() {
		return threeD;
	}

	public void setThreeD(Boolean threeD) {
		this.threeD = threeD;
	}

	public BigDecimal getBudget() {
		return budget;
	}

	public void setBudget(BigDecimal budget) {
		this.budget = budget;
	}

	public BigDecimal getProfitGeneral() {
		return profitGeneral;
	}

	public void setProfitGeneral(BigDecimal profitGeneral) {
		this.profitGeneral = profitGeneral;
	}

	public BigDecimal getOneWeekProfit() {
		return oneWeekProfit;
	}

	public void setOneWeekProfit(BigDecimal oneWeekProfit) {
		this.oneWeekProfit = oneWeekProfit;
	}

	public List<CastEntity> getCasts() {
		return casts;
	}

	public void setCasts(List<CastEntity> casts) {
		this.casts = casts;
	}

	public List<MoneyEntity> getMoney() {
		return money;
	}

	public void setMoney(List<MoneyEntity> money) {
		this.money = money;
	}

	public MovieEntity(String title, MovieCategory movieCategory, MovieType movieType, int runtime,
			LocalDate releaseDate, String country, Boolean threeD, BigDecimal budget, BigDecimal profitGeneral,
			BigDecimal oneWeekProfit) {
		super();
		this.title = title;
		this.movieCategory = movieCategory;
		this.movieType = movieType;
		this.runtime = runtime;
		this.releaseDate = releaseDate;
		this.country = country;
		this.threeD = threeD;
		this.budget = budget;
		this.profitGeneral = profitGeneral;
		this.oneWeekProfit = oneWeekProfit;
	}

}
