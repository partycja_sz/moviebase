package com.jstk.movie.service.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jstk.movie.criteria.MovieSearchCriteria;
import com.jstk.movie.dao.MoneyRepository;
import com.jstk.movie.dao.MovieRepository;
import com.jstk.movie.domain.MovieEntity;
import com.jstk.movie.exceptions.DuplicatedMovieHasBeenFoundException;
import com.jstk.movie.mappers.MovieMapper;
import com.jstk.movie.mappers.StudioMapper;
import com.jstk.movie.service.MovieService;
import com.jstk.movie.service.validator.MovieServiceValidator;
import com.jstk.movie.types.MoneyTO;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.StudioTO;

@Service
@Transactional(readOnly = false)
public class MovieServiceImpl implements MovieService {

	@Autowired
	private MovieRepository movieRepository;

	@Autowired
	private MoneyRepository moneyRepository;
	
	@Autowired
	private MovieMapper movieMapper;

	@Autowired
	private StudioMapper studioMapper;

	@Autowired
	private MovieServiceValidator movieServiceValidator;

	@Override
	public MovieTO addMovie(MovieTO movieTo) throws DuplicatedMovieHasBeenFoundException {
		if (!movieServiceValidator.validateMovieExistence(movieTo)) {
			MovieEntity movieAdded = movieRepository.save(movieMapper.movieTo2movieEntity(movieTo));
			return movieMapper.movieEntity2movieTo(movieAdded);
		} else {
			throw new DuplicatedMovieHasBeenFoundException();
		}
	}

	@Override
	public MovieTO updateMovie(MovieTO movieTo) {
		MovieEntity movieAdded = movieRepository.saveAndFlush(movieMapper.movieTo2movieEntity(movieTo));
		return movieMapper.movieEntity2movieTo(movieAdded);
	}

	@Override
	public MovieTO findMovieById(Long id) {
		Optional<MovieEntity> movieEntity = movieRepository.findById(id);
		if (movieEntity.isPresent()) {
			return movieMapper.movieEntity2movieTo(movieEntity.get());
		} else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public List<BigDecimal> findMoviesBudgetBetweenSpecifiedDates(int dateFrom, int dateTo) {
		LocalDate from = LocalDate.of(dateFrom, 1, 1);
		LocalDate to = LocalDate.of(dateTo, 12, 31);
		return movieRepository.findMoviesBudgetBetweenSpecifiedDates(from, to);
	}

	@Override
	public List<MovieEntity> findTheLongestMovieInSpecifiedStudioInSpecifiedTime(LocalDate dateFrom, LocalDate dateTo,
			StudioTO studioTo) {
		return movieRepository.findTheLongestMovieInSpecifiedStudioInSpecifiedTime(dateFrom, dateTo,
				studioMapper.studioTo2studioEntity(studioTo));
	}

	@Override
	public List<MovieEntity> findAllMoviesByCriteria(MovieSearchCriteria movieSearchCriteria) {
		return movieRepository.findAllMoviesByCriteria(movieSearchCriteria);
	}

	@Override
	public MovieTO addMoneyWeekToMovie(MovieTO addedMovie, MoneyTO moneyTo) {
		addedMovie.getMoney().add(moneyTo);
		return updateMovie(addedMovie);
	}

	@Override
	public Map<MovieEntity, BigDecimal> findWholeEarnMoneyForSpecifiedNoOfMovies(int noOfTopExpensiveMovies) {
		return movieRepository.findWholeEarnMoneyForSpecifiedNoOfMovies(noOfTopExpensiveMovies);
	}

	@Override
	public void deleteMovieById(Long id) {
		movieRepository.deleteById(id);
	}

	@Override
	public Long countMovies() {
		return movieRepository.count();
	}

	@Override
	public Long countMoneys() {
		return moneyRepository.count();
	}
}
