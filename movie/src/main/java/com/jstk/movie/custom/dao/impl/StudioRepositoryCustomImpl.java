package com.jstk.movie.custom.dao.impl;

import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.jstk.movie.custom.dao.StudioRepositoryCustom;
import com.jstk.movie.domain.QMovieEntity;
import com.jstk.movie.domain.QStudioEntity;
import com.jstk.movie.domain.StudioEntity;
import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.impl.JPAQueryFactory;
@Repository
public class StudioRepositoryCustomImpl implements StudioRepositoryCustom {

	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Map<StudioEntity, Long> findAmountMoviePerStudioInSpecifiedYear(int year) {
		QMovieEntity movie = QMovieEntity.movieEntity;
		QStudioEntity studio = QStudioEntity.studioEntity;
		JPAQueryFactory queryFactory = new JPAQueryFactory(this.entityManager);
		Expression<Long> valueColumn = movie.count();
		
		return queryFactory
				.from(studio, movie)
			    .where(movie.studio.id.eq(studio.id), movie.releaseDate.year().eq((Integer)year))
			    .groupBy(studio.id)
			    .transform(GroupBy.groupBy(studio).as(valueColumn));
	}
}
