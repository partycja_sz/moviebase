package com.jstk.movie.custom.dao.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.jstk.movie.criteria.MovieSearchCriteria;
import com.jstk.movie.custom.dao.MovieRepositoryCustom;
import com.jstk.movie.domain.MovieEntity;
import com.jstk.movie.domain.QMoneyEntity;
import com.jstk.movie.domain.QMovieEntity;
import com.jstk.movie.domain.QStudioEntity;
import com.jstk.movie.domain.StudioEntity;
import com.jstk.movie.mappers.MoneyMapper;
import com.jstk.movie.mappers.MovieMapper;
import com.jstk.movie.mappers.StudioMapper;
import com.querydsl.core.group.GroupBy;
import com.querydsl.core.types.Expression;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class MovieRepositoryCustomImpl implements MovieRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private StudioMapper studioMapper;

	@Autowired
	private MovieMapper movieMapper;

	@Autowired
	private MoneyMapper moneyMapper;

	@Override
	public List<BigDecimal> findMoviesBudgetBetweenSpecifiedDates(LocalDate dateFrom, LocalDate dateTo) {
		QMovieEntity movie = QMovieEntity.movieEntity;
		JPAQueryFactory queryFactory = new JPAQueryFactory(this.entityManager);

		return queryFactory.select(movie.budget).from(movie)
				.where(movie.releaseDate.between(dateFrom, dateTo)).fetch();
	}

	@Override
	public List<MovieEntity> findAllMoviesByCriteria(MovieSearchCriteria movieSearchCriteria) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MovieEntity> criteriaQuery = builder.createQuery(MovieEntity.class);
		Root<MovieEntity> movie = criteriaQuery.from(MovieEntity.class);
		List<Predicate> predicates = new ArrayList<>();

		if (movieSearchCriteria.getTitle() != null) {
			predicates.add(builder.equal(movie.get("title"), movieSearchCriteria.getTitle()));
		}
		if (movieSearchCriteria.getCountry() != null) {
			predicates.add(builder.equal(movie.get("country"), movieSearchCriteria.getCountry()));
		}
		if (movieSearchCriteria.getReleaseYear() != 0) {
			javax.persistence.criteria.Expression<?> year = (javax.persistence.criteria.Expression<?>) builder
					.function("year", Integer.class, movie.get("releaseDate"));
			predicates.add(builder.equal(year, movieSearchCriteria.getReleaseYear()));
		}
		if (movieSearchCriteria.getMovieCategory() != null) {
			predicates.add(builder.equal(movie.get("movieCategory"), movieSearchCriteria.getMovieCategory()));
		}
		if (movieSearchCriteria.getMovieType() != null) {
			predicates.add(builder.equal(movie.get("movieType"), movieSearchCriteria.getMovieType()));
		}
		if (movieSearchCriteria.getRuntimeFrom() != 0) {
			predicates.add(builder.ge(movie.get("runtime"), movieSearchCriteria.getRuntimeFrom()));
		}
		if (movieSearchCriteria.getRuntimeTo() != 0) {
			predicates.add(builder.le(movie.get("runtime"), movieSearchCriteria.getRuntimeTo()));
		}
		if (movieSearchCriteria.getReleaseDateFrom() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(movie.get("releaseDate"), movieSearchCriteria.getReleaseDateFrom()));
		}
		if (movieSearchCriteria.getReleaseDateTo() != null) {
			predicates.add(builder.lessThanOrEqualTo(movie.get("releaseDate"), movieSearchCriteria.getReleaseDateTo()));
		}
		if (movieSearchCriteria.getStudio() != null) {
			predicates.add(builder.equal(movie.get("studio"),
					studioMapper.studioTo2studioEntity(movieSearchCriteria.getStudio())));
		}
		if (movieSearchCriteria.getThreeD() != null) {
			predicates.add(builder.equal(movie.get("threeD"), movieSearchCriteria.getThreeD()));
		}
		criteriaQuery.select(movie).where(predicates.toArray(new Predicate[] {}));
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

	@Override
	public List<MovieEntity> findTheLongestMovieInSpecifiedStudioInSpecifiedTime(LocalDate dateFrom, LocalDate dateTo,
			StudioEntity studioEntity) {
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setStudio(studioMapper.studioEntity2studioTo(studioEntity));
		movieSearchCriteria.setReleaseDateFrom(dateFrom);
		movieSearchCriteria.setReleaseDateTo(dateTo);

		QMovieEntity movie = QMovieEntity.movieEntity;
		QStudioEntity studio = QStudioEntity.studioEntity;
		JPAQueryFactory queryFactory = new JPAQueryFactory(this.entityManager);

		return queryFactory.select(movie).from(movie).join(movie.studio, studio)
				.where(movie.releaseDate.between(dateFrom, dateTo), studio.eq(studioEntity))
				.orderBy(movie.runtime.desc()).limit(1).fetch();
	}

	@Override
	public Map<MovieEntity, BigDecimal> findWholeEarnMoneyForSpecifiedNoOfMovies(int noOfTopExpensiveMovies) {
		QMovieEntity movie = QMovieEntity.movieEntity;
		QMoneyEntity money = QMoneyEntity.moneyEntity;
		JPAQueryFactory queryFactory = new JPAQueryFactory(this.entityManager);
		Expression<BigDecimal> wholeEarnedMoneyColumn = money.weekProfit.sum();

		return queryFactory.from(movie, money).where(movie.money.contains(money))
				.groupBy(movie.id).orderBy(movie.budget.desc()).limit(noOfTopExpensiveMovies)
				.transform(GroupBy.groupBy(movie).as(wholeEarnedMoneyColumn));
	}
}
