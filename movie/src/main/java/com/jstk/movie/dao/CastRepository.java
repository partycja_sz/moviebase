package com.jstk.movie.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jstk.movie.custom.dao.CastRepositoryCustom;
import com.jstk.movie.domain.CastEntity;

public interface CastRepository extends JpaRepository<CastEntity, Long>, CastRepositoryCustom {

}
