package com.jstk.movie.types;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class MoneyTO {
	private Long id;
	private Long version;
	private LocalDateTime creationDate;
	private LocalDateTime modyficationDate;
	private BigDecimal weekProfit;
	private int noOfWeek;

	public MoneyTO(Long id, Long version, LocalDateTime creationDate, LocalDateTime modyficationDate,
			BigDecimal weekProfit, int noOfWeek) {
		super();
		this.id = id;
		this.version = version;
		this.creationDate = creationDate;
		this.modyficationDate = modyficationDate;
		this.weekProfit = weekProfit;
		this.noOfWeek = noOfWeek;
	}

	public MoneyTO(Long id, BigDecimal weekProfit, int noOfWeek) {
		super();
		this.id = id;
		this.weekProfit = weekProfit;
		this.noOfWeek = noOfWeek;
	}

	public MoneyTO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public LocalDateTime getModyficationDate() {
		return modyficationDate;
	}

	public void setModyficationDate(LocalDateTime modyficationDate) {
		this.modyficationDate = modyficationDate;
	}

	public BigDecimal getWeekProfit() {
		return weekProfit;
	}

	public void setWeekProfit(BigDecimal weekProfit) {
		this.weekProfit = weekProfit;
	}

	public int getNoOfWeek() {
		return noOfWeek;
	}

	public void setNoOfWeek(int noOfWeek) {
		this.noOfWeek = noOfWeek;
	}

	public static MoneyTOBuilder builder() {
		return new MoneyTOBuilder();
	}

	public static class MoneyTOBuilder {
		private Long id;
		private BigDecimal weekProfit;
		private int noOfWeek;

		public MoneyTOBuilder() {
			super();
		}

		public MoneyTOBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public MoneyTOBuilder withWeekProfit(BigDecimal weekProfit) {
			this.weekProfit = weekProfit;
			return this;
		}

		public MoneyTOBuilder withNoOfWeek(int noOfWeek) {
			this.noOfWeek = noOfWeek;
			return this;
		}

		public MoneyTO build() {
			checkBeforeBuild(weekProfit, noOfWeek);
			return new MoneyTO(id, weekProfit, noOfWeek);
		}

		private void checkBeforeBuild(BigDecimal weekProfit, int noOfWeek) {
			if (weekProfit == null || noOfWeek == 0) {
				throw new IllegalArgumentException("Incorrect moeny to be created");
			}
		}
	}
}
