package com.jstk.movie.service.impl;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jstk.movie.dao.MovieRepository;
import com.jstk.movie.dao.StudioRepository;
import com.jstk.movie.domain.StudioEntity;
import com.jstk.movie.mappers.MovieMapper;
import com.jstk.movie.mappers.StudioMapper;
import com.jstk.movie.service.StudioService;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.StudioTO;

@Service
@Transactional
public class StudioServiceImpl implements StudioService {

	@Autowired
	private StudioRepository studioRepository;

	@Autowired
	private MovieRepository movieRepository;

	@Autowired
	private StudioMapper studioMapper;

	@Autowired
	private MovieMapper movieMapper;

	@Override
	public StudioTO addStudio(StudioTO studioTo) {
		StudioEntity studioEntity = studioRepository.save(studioMapper.studioTo2studioEntity(studioTo));
		return studioMapper.studioEntity2studioTo(studioEntity);
	}

	@Override
	public void removeStudio(StudioTO addedStudio) {
		studioRepository.deleteById(addedStudio.getId());
	}

	@Override
	public long countAllStudios() {
		return studioRepository.count();
	}

	@Override
	public StudioTO updateStudio(StudioTO studioTo) {
		StudioEntity studioEntity = studioRepository.saveAndFlush(studioMapper.studioTo2studioEntity(studioTo));

		return studioMapper.studioEntity2studioTo(studioEntity);
	}

	@Override
	public StudioTO findStudioById(Long id) {
		Optional<StudioEntity> studioEntity = studioRepository.findById(id);
		if (studioEntity.isPresent()) {
			return studioMapper.studioEntity2studioTo(studioEntity.get());
		} else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public Map<StudioEntity, Long> findAmountMoviePerStudioInSpecifiedYear(int year) {
		return studioRepository.findAmountMoviePerStudioInSpecifiedYear(year);
	}

	@Override
	public void addMovieToStudio(StudioTO addedStudio, MovieTO addedMovie) {
		addedStudio.getMovies().add(movieMapper.movieTo2movieEntity(addedMovie));
		addedMovie.setStudio(addedStudio);
		studioRepository.save(studioMapper.studioTo2studioEntity(addedStudio));
	}
}
