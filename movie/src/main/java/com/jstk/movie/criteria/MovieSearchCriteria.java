package com.jstk.movie.criteria;

import java.time.LocalDate;
import com.jstk.movie.enums.MovieCategory;
import com.jstk.movie.enums.MovieType;
import com.jstk.movie.types.StudioTO;

public class MovieSearchCriteria {
	private String title;
	private String country;
	private int releaseYear;
	private MovieCategory movieCategory;
	private MovieType movieType;
	private int runtimeFrom;
	private int runtimeTo;
	private LocalDate releaseDateFrom;
	private LocalDate releaseDateTo;
	private Boolean threeD;
	private StudioTO studio;

	public MovieSearchCriteria() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(LocalDate releaseYear) {
		this.releaseYear = releaseYear.getYear();
	}

	public MovieCategory getMovieCategory() {
		return movieCategory;
	}

	public void setMovieCategory(MovieCategory movieCategory) {
		this.movieCategory = movieCategory;
	}

	public MovieType getMovieType() {
		return movieType;
	}

	public void setMovieType(MovieType movieType) {
		this.movieType = movieType;
	}

	public int getRuntimeFrom() {
		return runtimeFrom;
	}

	public void setRuntimeFrom(int runtimeFrom) {
		this.runtimeFrom = runtimeFrom;
	}

	public int getRuntimeTo() {
		return runtimeTo;
	}

	public void setRuntimeTo(int runtimeTo) {
		this.runtimeTo = runtimeTo;
	}

	public LocalDate getReleaseDateFrom() {
		return releaseDateFrom;
	}

	public void setReleaseDateFrom(LocalDate releaseDateFrom) {
		this.releaseDateFrom = releaseDateFrom;
	}

	public LocalDate getReleaseDateTo() {
		return releaseDateTo;
	}

	public void setReleaseDateTo(LocalDate releaseDateTo) {
		this.releaseDateTo = releaseDateTo;
	}

	public Boolean getThreeD() {
		return threeD;
	}

	public void setThreeD(Boolean threeD) {
		this.threeD = threeD;
	}

	public StudioTO getStudio() {
		return studio;
	}

	public void setStudio(StudioTO studio) {
		this.studio = studio;
	}
}
