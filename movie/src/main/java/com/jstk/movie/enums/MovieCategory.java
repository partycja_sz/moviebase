package com.jstk.movie.enums;

public enum MovieCategory {
	COMEDY, THRILLER, CRIME, DRAMA
}
