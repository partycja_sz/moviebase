package com.jstk.movie;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.jstk.movie.service.CastService;
import com.jstk.movie.types.CastTO;
import com.jstk.movie.types.CastTO.CastTOBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class CastServiceTests {

	@Autowired
	private CastService castService;

	@Test
	public void addCastTest() throws ParseException {

		// given
		String dateRelaseString = "2003-05-15";
		String endRelaseString = "2013-05-15";
		LocalDate startCooperation = LocalDate.parse(dateRelaseString);
		LocalDate endCooperation = LocalDate.parse(endRelaseString);

		String firstName = "Adam";
		String lastName = "Ray";

		CastTO castTo = new CastTOBuilder().withFirstName(firstName).withLastName(lastName)
				.withStartCooperationDate(startCooperation).withEndCooperationDate(endCooperation).build();

		// when
		CastTO addedCast = castService.addCast(castTo);

		// then
		assertEquals(firstName, addedCast.getFirstName());
		assertEquals(lastName, addedCast.getLastName());
		assertNotNull(addedCast.getCreationDate());
		assertTrue(addedCast.getVersion().equals(0L));
	}

	@Test
	public void findByIdCastTest() throws ParseException {

		// given
		String dateRelaseString = "2003-05-15";
		String endRelaseString = "2013-05-15";
		LocalDate startCooperation = LocalDate.parse(dateRelaseString);
		LocalDate endCooperation = LocalDate.parse(endRelaseString);

		String firstName = "Adam";
		String lastName = "Ray";

		CastTO castTo = new CastTOBuilder().withFirstName(firstName).withLastName(lastName)
				.withStartCooperationDate(startCooperation).withEndCooperationDate(endCooperation).build();
		CastTO addedCast = castService.addCast(castTo);

		// when
		CastTO foundCast = castService.findCastById(addedCast.getId());

		// then
		assertEquals(firstName, foundCast.getFirstName());
		assertEquals(lastName, foundCast.getLastName());
		assertNotNull(foundCast.getCreationDate());
		assertTrue(foundCast.getVersion().equals(0L));
	}

	@Test
	public void updateCastTest() throws ParseException {

		// given
		String dateRelaseString = "2003-05-15";
		String endRelaseString = "2013-05-15";
		LocalDate startCooperation = LocalDate.parse(dateRelaseString);
		LocalDate endCooperation = LocalDate.parse(endRelaseString);

		String firstName = "Adam";
		String lastName = "Ray";
		String lastNameUpdated = "Ray The Great";

		CastTO castTo = new CastTOBuilder().withFirstName(firstName).withLastName(lastName)
				.withStartCooperationDate(startCooperation).withEndCooperationDate(endCooperation).build();
		CastTO addedCast = castService.addCast(castTo);

		// when
		addedCast.setLastName(lastNameUpdated);
		CastTO updatedCast = castService.updateCast(addedCast);

		// then
		assertEquals(firstName, updatedCast.getFirstName());
		assertEquals(lastNameUpdated, updatedCast.getLastName());
		assertNotNull(updatedCast.getCreationDate());
		assertTrue(updatedCast.getModyficationDate().compareTo(updatedCast.getCreationDate()) >= 0);
		assertTrue(updatedCast.getVersion().equals(1L));
	}
}
