package com.jstk.movie;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MovieServiceTests.class, CastServiceTests.class, StudioServiceTests.class, QueryTests.class, OptimisticLockTests.class })
public class ServiceTestSuite {
}
