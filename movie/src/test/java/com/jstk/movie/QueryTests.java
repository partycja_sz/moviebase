package com.jstk.movie;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.jstk.movie.criteria.MovieSearchCriteria;
import com.jstk.movie.domain.MovieEntity;
import com.jstk.movie.domain.StudioEntity;
import com.jstk.movie.enums.MovieCategory;
import com.jstk.movie.enums.MovieType;
import com.jstk.movie.exceptions.DuplicatedMovieHasBeenFoundException;
import com.jstk.movie.service.MovieService;
import com.jstk.movie.service.StudioService;
import com.jstk.movie.types.MoneyTO;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.MovieTO.MovieTOBuilder;
import com.jstk.movie.types.StudioTO;
import com.jstk.movie.types.MoneyTO.MoneyTOBuilder;
import com.jstk.movie.types.StudioTO.StudioTOBuilder;;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class QueryTests {
	@Autowired
	private MovieService movieService;

	@Autowired
	private StudioService studioService;

	DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	MovieCategory movieCategoryThiriller;
	MovieCategory movieCategoryDrama;
	MovieCategory movieCategoryComedy;
	MovieType movieTypeColor;
	MovieType movieTypeBlcakWhite;
	String countryUSA;
	String countryGermany;
	String countrySpain;
	Boolean isThreeDMovie;
	Boolean yesThreeDMovie;
	BigDecimal profitGeneral;
	BigDecimal oneWeekProfit;
	int runtime1;
	int runtime2;
	int runtime3;
	int runtime4;
	int runtime5;
	int runtime6;
	String dateRelaseString1;
	String dateRelaseString2;
	String dateRelaseString3;
	String dateRelaseString4;
	String dateRelaseString5;
	String dateRelaseString6;
	String dateRelaseString7;
	String dateRelaseString8;
	String dateRelaseString9;
	String dateRelaseString10;

	LocalDate releaseDate1;
	LocalDate releaseDate2;
	LocalDate releaseDate3;
	LocalDate releaseDate4;
	LocalDate releaseDate5;
	LocalDate releaseDate6;
	LocalDate releaseDate7;
	LocalDate releaseDate8;
	LocalDate releaseDate9;
	LocalDate releaseDate10;

	BigDecimal budget1;
	BigDecimal budget2;
	BigDecimal budget3;
	BigDecimal budget4;

	String title1;
	String title2;
	String title3;
	String title4;
	String title5;
	String title6;
	String title7;
	String title8;
	String title9;
	String title10;

	MovieTO movie1;
	MovieTO addedMovie1;
	MovieTO movie2;
	MovieTO addedMovie2;
	MovieTO movie3;
	MovieTO addedMovie3;
	MovieTO movie4;
	MovieTO addedMovie4;
	MovieTO movie5;
	MovieTO addedMovie5;
	MovieTO movie6;
	MovieTO addedMovie6;
	MovieTO movie7;
	MovieTO addedMovie7;
	MovieTO movie8;
	MovieTO addedMovie8;
	MovieTO movie9;
	MovieTO addedMovie9;
	MovieTO movie10;
	MovieTO addedMovie10;

	StudioTO studioTo1;
	StudioTO addedStudio1;
	String addressStudio1;
	String nameStudio1;
	StudioTO studioTo2;
	StudioTO addedStudio2;
	String addressStudio2;
	String nameStudio2;
	StudioTO studioTo3;
	StudioTO addedStudio3;
	String addressStudio3;
	String nameStudio3;
	int week1;
	int week2;
	int week3;
	int week4;
	int week5;
	BigDecimal profitWeek1;
	BigDecimal profitWeek2;
	BigDecimal profitWeek3;
	BigDecimal profitWeek4;
	BigDecimal profitWeek5;
	BigDecimal profitWeek6;
	BigDecimal profitWeek7;
	BigDecimal profitWeek8;
	BigDecimal profitWeek9;
	BigDecimal profitWeek10;
	MoneyTO moneyTo1;
	MoneyTO moneyTo2;
	MoneyTO moneyTo3;
	MoneyTO moneyTo4;
	MoneyTO moneyTo5;
	MoneyTO moneyTo6;
	MoneyTO moneyTo7;
	MoneyTO moneyTo8;
	MoneyTO moneyTo9;
	MoneyTO moneyTo10;
	MoneyTO moneyTo11;
	MoneyTO moneyTo12;
	MoneyTO moneyTo13;
	MoneyTO moneyTo14;
	MoneyTO moneyTo15;
	MoneyTO moneyTo16;
	MoneyTO moneyTo17;
	MoneyTO moneyTo18;
	MoneyTO moneyTo19;
	MoneyTO moneyTo20;
	MoneyTO addedMoneyTo1;
	MoneyTO addedMoneyTo2;
	MoneyTO addedMoneyTo3;
	MoneyTO addedMoneyTo4;
	MoneyTO addedMoneyTo5;
	MoneyTO addedMoneyTo6;
	MoneyTO addedMoneyTo7;
	MoneyTO addedMoneyTo8;
	MoneyTO addedMoneyTo9;
	MoneyTO addedMoneyTo10;
	MoneyTO addedMoneyTo11;
	MoneyTO addedMoneyTo12;
	MoneyTO addedMoneyTo13;
	MoneyTO addedMoneyTo14;
	MoneyTO addedMoneyTo15;
	MoneyTO addedMoneyTo16;
	MoneyTO addedMoneyTo17;
	MoneyTO addedMoneyTo18;
	MoneyTO addedMoneyTo19;
	MoneyTO addedMoneyTo20;

	@Before
	public void setUpTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		movieCategoryThiriller = MovieCategory.THRILLER;
		movieCategoryDrama = MovieCategory.DRAMA;
		movieCategoryComedy = MovieCategory.COMEDY;
		movieTypeColor = MovieType.COLOR;
		movieTypeBlcakWhite = MovieType.BLACKWHITE;
		countryUSA = "USA";
		countryGermany = "Germany";
		countrySpain = "Spain";
		isThreeDMovie = false;
		yesThreeDMovie = true;
		profitGeneral = new BigDecimal("521000.12");
		oneWeekProfit = new BigDecimal("94512.01");
		runtime1 = 190;
		runtime2 = 185;
		runtime3 = 160;
		runtime4 = 132;
		runtime5 = 155;
		runtime6 = 240;
		title1 = "Tombstone";
		title2 = "Breakfast at Tiffany";
		title3 = "Great Gatsby";
		title4 = "King";
		title5 = "Run";
		title6 = "Avatar";
		title7 = "War";
		title8 = "Smith";
		title9 = "Great King";
		title10 = "Warsaw";

		dateRelaseString1 = "2004-01-15";
		releaseDate1 = LocalDate.parse(dateRelaseString1);
		dateRelaseString2 = "2008-09-19";
		releaseDate2 = LocalDate.parse(dateRelaseString2);
		dateRelaseString3 = "2009-01-16";
		releaseDate3 = LocalDate.parse(dateRelaseString3);
		dateRelaseString4 = "2010-01-21";
		releaseDate4 = LocalDate.parse(dateRelaseString4);
		dateRelaseString5 = "2010-01-21";
		releaseDate5 = LocalDate.parse(dateRelaseString5);
		dateRelaseString6 = "2011-01-21";
		releaseDate6 = LocalDate.parse(dateRelaseString6);
		dateRelaseString7 = "2011-02-12";
		releaseDate7 = LocalDate.parse(dateRelaseString7);
		dateRelaseString8 = "2011-11-04";
		releaseDate8 = LocalDate.parse(dateRelaseString8);
		dateRelaseString9 = "2011-12-02";
		releaseDate9 = LocalDate.parse(dateRelaseString9);
		dateRelaseString10 = "2011-01-23";
		releaseDate10 = LocalDate.parse(dateRelaseString10);
		budget1 = new BigDecimal("253456.89");
		budget2 = new BigDecimal("123456.89");
		budget3 = new BigDecimal("4000000.00");
		budget4 = new BigDecimal("1200015.00");

		addressStudio1 = "studio1 address";
		nameStudio1 = "Universal";
		addressStudio2 = "studio2 address";
		nameStudio2 = "Columbia";
		addressStudio3 = "studio3 address";
		nameStudio3 = "Next";

		int week1 = 1;
		int week2 = 2;
		int week3 = 3;
		int week4 = 4;
		int week5 = 5;
		profitWeek1 = new BigDecimal("15123.01");
		profitWeek2 = new BigDecimal("99999.90");
		profitWeek3 = new BigDecimal("150000");
		profitWeek4 = new BigDecimal("120");
		profitWeek5 = new BigDecimal("85120");
		profitWeek6 = new BigDecimal("1500");
		profitWeek7 = new BigDecimal("56941");
		profitWeek8 = new BigDecimal("22224");
		profitWeek9 = new BigDecimal("74585");
		profitWeek10 = new BigDecimal("88888.88");

		studioTo1 = new StudioTOBuilder().withAddress(addressStudio1).withName(nameStudio1).build();
		addedStudio1 = studioService.addStudio(studioTo1);

		studioTo2 = new StudioTOBuilder().withAddress(addressStudio2).withName(nameStudio2).build();
		addedStudio2 = studioService.addStudio(studioTo2);

		studioTo3 = new StudioTOBuilder().withAddress(addressStudio3).withName(nameStudio3).build();
		addedStudio3 = studioService.addStudio(studioTo3);

		movie1 = new MovieTOBuilder().withTitle(title1).withThreeD(isThreeDMovie).withRuntime(runtime1)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeColor).withCountry(countryUSA)
				.withBudget(budget1).withReleaseDate(releaseDate1).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio1).build();
		addedMovie1 = movieService.addMovie(movie1);

		movie2 = new MovieTOBuilder().withTitle(title2).withThreeD(isThreeDMovie).withRuntime(runtime2)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeColor).withCountry(countryGermany)
				.withBudget(budget2).withReleaseDate(releaseDate2).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio2).build();
		addedMovie2 = movieService.addMovie(movie2);

		movie3 = new MovieTOBuilder().withTitle(title3).withThreeD(isThreeDMovie).withRuntime(runtime3)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeColor).withCountry(countryUSA)
				.withBudget(budget3).withReleaseDate(releaseDate3).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio1).build();
		addedMovie3 = movieService.addMovie(movie3);

		movie4 = new MovieTOBuilder().withTitle(title4).withThreeD(isThreeDMovie).withRuntime(runtime4)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeBlcakWhite).withCountry(countrySpain)
				.withBudget(budget4).withReleaseDate(releaseDate4).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio2).build();
		addedMovie4 = movieService.addMovie(movie4);

		movie5 = new MovieTOBuilder().withTitle(title5).withThreeD(isThreeDMovie).withRuntime(runtime4)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeColor).withCountry(countrySpain)
				.withBudget(budget4).withReleaseDate(releaseDate5).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio1).build();
		addedMovie5 = movieService.addMovie(movie5);

		movie6 = new MovieTOBuilder().withTitle(title6).withThreeD(yesThreeDMovie).withRuntime(runtime4)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeColor).withCountry(countrySpain)
				.withBudget(budget4).withReleaseDate(releaseDate6).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio2).build();
		addedMovie6 = movieService.addMovie(movie6);

		movie7 = new MovieTOBuilder().withTitle(title7).withThreeD(yesThreeDMovie).withRuntime(runtime5)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeColor).withCountry(countrySpain)
				.withBudget(budget4).withReleaseDate(releaseDate7).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio2).build();
		addedMovie7 = movieService.addMovie(movie7);

		movie8 = new MovieTOBuilder().withTitle(title8).withThreeD(isThreeDMovie).withRuntime(runtime4)
				.withMovieCategory(movieCategoryDrama).withMovieType(movieTypeColor).withCountry(countrySpain)
				.withBudget(budget4).withReleaseDate(releaseDate8).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio2).build();
		addedMovie8 = movieService.addMovie(movie8);

		movie9 = new MovieTOBuilder().withTitle(title9).withThreeD(isThreeDMovie).withRuntime(runtime6)
				.withMovieCategory(movieCategoryComedy).withMovieType(movieTypeColor).withCountry(countrySpain)
				.withBudget(budget4).withReleaseDate(releaseDate9).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio2).build();
		addedMovie9 = movieService.addMovie(movie9);

		movie10 = new MovieTOBuilder().withTitle(title10).withThreeD(isThreeDMovie).withRuntime(runtime1)
				.withMovieCategory(movieCategoryThiriller).withMovieType(movieTypeColor).withCountry(countrySpain)
				.withBudget(budget4).withReleaseDate(releaseDate10).withOneWeekProfit(oneWeekProfit)
				.withProfitGeneral(profitGeneral).withStudio(addedStudio1).build();
		addedMovie10 = movieService.addMovie(movie10);

		moneyTo1 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek1).build();
		moneyTo2 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek2).build();
		moneyTo3 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek3).build();
		moneyTo4 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek4).build();
		moneyTo5 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek5).build();
		moneyTo6 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek6).build();
		moneyTo7 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek7).build();
		moneyTo8 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek8).build();
		moneyTo9 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek9).build();
		moneyTo10 = new MoneyTOBuilder().withNoOfWeek(week1).withWeekProfit(profitWeek10).build();
		moneyTo11 = new MoneyTOBuilder().withNoOfWeek(week2).withWeekProfit(profitWeek1).build();
		moneyTo12 = new MoneyTOBuilder().withNoOfWeek(week3).withWeekProfit(profitWeek2).build();
		moneyTo13 = new MoneyTOBuilder().withNoOfWeek(week3).withWeekProfit(profitWeek3).build();
		moneyTo14 = new MoneyTOBuilder().withNoOfWeek(week4).withWeekProfit(profitWeek4).build();
		moneyTo15 = new MoneyTOBuilder().withNoOfWeek(week4).withWeekProfit(profitWeek5).build();
		moneyTo16 = new MoneyTOBuilder().withNoOfWeek(week5).withWeekProfit(profitWeek6).build();
		moneyTo17 = new MoneyTOBuilder().withNoOfWeek(week5).withWeekProfit(profitWeek7).build();
		moneyTo18 = new MoneyTOBuilder().withNoOfWeek(week2).withWeekProfit(profitWeek8).build();
		moneyTo19 = new MoneyTOBuilder().withNoOfWeek(week3).withWeekProfit(profitWeek9).build();
		moneyTo20 = new MoneyTOBuilder().withNoOfWeek(week2).withWeekProfit(profitWeek10).build();

		addedMovie1 = movieService.addMoneyWeekToMovie(addedMovie1, moneyTo1);
		addedMovie2 = movieService.addMoneyWeekToMovie(addedMovie2, moneyTo2);
		addedMovie3 = movieService.addMoneyWeekToMovie(addedMovie3, moneyTo3);
		addedMovie4 = movieService.addMoneyWeekToMovie(addedMovie4, moneyTo4);
		addedMovie5 = movieService.addMoneyWeekToMovie(addedMovie5, moneyTo5);
		addedMovie6 = movieService.addMoneyWeekToMovie(addedMovie6, moneyTo6);
		addedMovie7 = movieService.addMoneyWeekToMovie(addedMovie7, moneyTo7);
		addedMovie8 = movieService.addMoneyWeekToMovie(addedMovie8, moneyTo8);
		addedMovie9 = movieService.addMoneyWeekToMovie(addedMovie9, moneyTo9);
		addedMovie10 = movieService.addMoneyWeekToMovie(addedMovie10, moneyTo10);
		addedMovie6 = movieService.addMoneyWeekToMovie(addedMovie6, moneyTo11);
		addedMovie7 = movieService.addMoneyWeekToMovie(addedMovie7, moneyTo12);
		addedMovie8 = movieService.addMoneyWeekToMovie(addedMovie8, moneyTo13);
		addedMovie9 = movieService.addMoneyWeekToMovie(addedMovie9, moneyTo14);
		addedMovie6 = movieService.addMoneyWeekToMovie(addedMovie6, moneyTo15);
		addedMovie7 = movieService.addMoneyWeekToMovie(addedMovie7, moneyTo16);
		addedMovie8 = movieService.addMoneyWeekToMovie(addedMovie8, moneyTo17);
		addedMovie6 = movieService.addMoneyWeekToMovie(addedMovie6, moneyTo18);
		addedMovie7 = movieService.addMoneyWeekToMovie(addedMovie7, moneyTo19);
		addedMovie8 = movieService.addMoneyWeekToMovie(addedMovie8, moneyTo20);
	}

	@Test
	public void checkMovieBudgetBetweenSpecifiedDatesTest()
			throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		int dateFrom = 2005;
		int dateTo = 2010;

		// when
		List<BigDecimal> movieBudgetList = movieService.findMoviesBudgetBetweenSpecifiedDates(dateFrom, dateTo);

		// then
		assertNotNull(movieBudgetList);
		assertTrue(movieBudgetList.size() == 4);
	}

	@Test
	public void checkIfTheLongestMovieIsFoundWhenSpecifiedStudioAndSpecifiedDatesAreSetTest() throws ParseException {

		// given
		String dateFromString = "2004-01-15";
		LocalDate dateFrom = LocalDate.parse(dateFromString);
		String dateToString = "2010-01-19";
		LocalDate dateTo = LocalDate.parse(dateToString);

		// when
		List<MovieEntity> movieBudgetList = movieService.findTheLongestMovieInSpecifiedStudioInSpecifiedTime(dateFrom,
				dateTo, addedStudio1);

		// then
		assertNotNull(movieBudgetList);

	}

	@Test
	public void findAmountMoviePerStudioInSpecifiedYearTest() {
		// given
		int yearToCheck = 2011;

		// when
		Map<StudioEntity, Long> studioMap = studioService.findAmountMoviePerStudioInSpecifiedYear(yearToCheck);

		// then
		// TODO
		assertFalse(studioMap.isEmpty());

	}

	@Test
	public void checkEarnedMoneyForSpecifiedNoOfMoviesTest() {
		// given
		int noOfTopExpensiveMovies = 3;

		// when
		Map<MovieEntity, BigDecimal> movieMap = movieService
				.findWholeEarnMoneyForSpecifiedNoOfMovies(noOfTopExpensiveMovies);

		// then
		assertFalse(movieMap.isEmpty());
		assertTrue(movieMap.size() == noOfTopExpensiveMovies);
	}

	@Test
	public void findMovieByCategoryDramaTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setMovieCategory(movieCategoryDrama);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertEquals(movieCategoryDrama, movie.getMovieCategory());
		}
	}

	@Test
	public void findMovieByTypeBlackWhiteTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setMovieType(movieTypeBlcakWhite);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertEquals(movieTypeBlcakWhite, movie.getMovieType());
		}
	}

	@Test
	public void find3DmovieTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setThreeD(yesThreeDMovie);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertEquals(yesThreeDMovie, movie.getThreeD());
		}
	}

	@Test
	public void findMovieByStudioTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setStudio(addedStudio2);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertEquals(addedStudio2.getId(), movie.getStudio().getId());
		}
	}

	@Test
	public void findMovieShorterThanSpecifiedRuntimeTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setRuntimeTo(runtime3);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertTrue(movie.getRuntime() <= runtime3);
		}
	}

	@Test
	public void findMovieLongerThanSpecifiedRuntimeTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setRuntimeFrom(runtime2);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertTrue(movie.getRuntime() >= runtime2);
		}
	}

	@Test
	public void findMovieRealesedEalierThanSpecifiedDateTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setReleaseDateTo(releaseDate6);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertTrue((movie.getReleaseDate()).compareTo(releaseDate6) <= 0);
		}
	}

	@Test
	public void findMovieRealesedLaterThanSpecifiedDateTest() {
		// given
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setReleaseDateFrom(releaseDate6);

		// when
		List<MovieEntity> movieList = movieService.findAllMoviesByCriteria(movieSearchCriteria);

		// then
		assertFalse(movieList.isEmpty());
		for (MovieEntity movie : movieList) {
			assertTrue((movie.getReleaseDate()).compareTo(releaseDate6) >= 0);
		}
	}
}
