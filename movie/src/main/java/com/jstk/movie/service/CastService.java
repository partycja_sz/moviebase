package com.jstk.movie.service;

import com.jstk.movie.types.CastTO;

public interface CastService {

	CastTO addCast(CastTO castTo);

	CastTO updateCast(CastTO castTo);

	CastTO findCastById(Long id);
}
