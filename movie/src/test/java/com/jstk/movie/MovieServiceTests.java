package com.jstk.movie;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.jstk.movie.enums.MovieCategory;
import com.jstk.movie.enums.MovieType;
import com.jstk.movie.exceptions.DuplicatedMovieHasBeenFoundException;
import com.jstk.movie.service.MovieService;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.MoneyTO;
import com.jstk.movie.types.MovieTO.MovieTOBuilder;
import com.jstk.movie.types.MoneyTO.MoneyTOBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class MovieServiceTests {

	@Autowired
	private MovieService movieService;

	DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Test
	public void addMovieTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		// when
		MovieTO movieTo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();

		MovieTO addedMovie = movieService.addMovie(movieTo);

		// then
		assertNotNull(addedMovie.getCreationDate());
		assertTrue(addedMovie.getVersion().equals(0L));
		assertNotNull(addedMovie.getId());
		assertEquals(title, addedMovie.getTitle());
		assertEquals(movieCategory, addedMovie.getMovieCategory());
		assertEquals(movieType, addedMovie.getMovieType());
		assertEquals(threeD, addedMovie.getThreeD());
		assertEquals(country, addedMovie.getCountry());
		assertEquals(runtime, addedMovie.getRuntime());
		assertEquals(releaseDate, addedMovie.getReleaseDate());
		assertEquals(budget, addedMovie.getBudget());
		assertEquals(oneWeekProfit, addedMovie.getOneWeekProfit());
		assertEquals(profitGeneral, addedMovie.getProfitGeneral());
	}

	@Test
	public void updateMovieTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		String titleUpdated = "Gladiator new version";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		MovieTO movieTo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();

		MovieTO addedMovie = movieService.addMovie(movieTo);

		// when
		addedMovie.setTitle(titleUpdated);
		MovieTO updatedMovie = movieService.updateMovie(addedMovie);
		MovieTO entityMovie = movieService.findMovieById(updatedMovie.getId());

		// then
		assertEquals(titleUpdated, entityMovie.getTitle());
		assertEquals(addedMovie.getId(), entityMovie.getId());
		assertTrue(entityMovie.getModyficationDate().compareTo(entityMovie.getCreationDate()) >= 0);
		assertTrue(entityMovie.getVersion().equals(1L));

	}

	@Test
	public void findByIdMovieTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		MovieTO movieTo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();

		MovieTO addedMovie = movieService.addMovie(movieTo);

		// when
		MovieTO entityMovie = movieService.findMovieById(addedMovie.getId());

		// then
		assertNotNull(entityMovie.getCreationDate());
		assertTrue(entityMovie.getVersion().equals(0L));
		assertNotNull(entityMovie.getId());
		assertEquals(title, entityMovie.getTitle());
		assertEquals(movieCategory, entityMovie.getMovieCategory());
		assertEquals(movieType, entityMovie.getMovieType());
		assertEquals(threeD, entityMovie.getThreeD());
		assertEquals(country, entityMovie.getCountry());
		assertEquals(runtime, entityMovie.getRuntime());
		assertEquals(releaseDate, entityMovie.getReleaseDate());
		assertEquals(budget, entityMovie.getBudget());
		assertEquals(oneWeekProfit, entityMovie.getOneWeekProfit());
		assertEquals(profitGeneral, entityMovie.getProfitGeneral());

	}

	@Test(expected = DuplicatedMovieHasBeenFoundException.class)
	public void checkAgainstDuplicationMovieTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		// when
		MovieTO movieOne = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();

		movieService.addMovie(movieOne);

		// then
		movieService.addMovie(movieOne);
	}

	@Test
	public void checkIfMovieSameTitleSameCountryDifferentYearCanBeAddedMovieTest()
			throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";
		String dateRelaseString2 = "2005-05-15";

		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		LocalDate releaseDate2 = LocalDate.parse(dateRelaseString2, dateFormatter);

		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		// when
		MovieTO movieOne = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();

		MovieTO movieTwo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate2).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral)
				.build();

		movieService.addMovie(movieOne);

		// then
		movieService.addMovie(movieTwo);

	}

	@Test
	public void addMoneyToMovieTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");
		BigDecimal profit1 = new BigDecimal("20000");

		MovieTO movieTo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();
		MovieTO addedMovie = movieService.addMovie(movieTo);

		// when
		MoneyTO moneyTo = new MoneyTOBuilder().withNoOfWeek(1).withWeekProfit(profit1).build();
		MovieTO updatedMovie = movieService.addMoneyWeekToMovie(addedMovie, moneyTo);

		// then
		assertFalse(updatedMovie.getMoney().isEmpty());
	}

	@Test
	public void removeMovieWithMoneyTest() throws ParseException, DuplicatedMovieHasBeenFoundException {
		// given
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");
		BigDecimal profit1 = new BigDecimal("20000");

		MovieTO movieTo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();
		MovieTO addedMovie = movieService.addMovie(movieTo);

		MoneyTO moneyTo = new MoneyTOBuilder().withNoOfWeek(1).withWeekProfit(profit1).build();
		MovieTO updatedMovie = movieService.addMoneyWeekToMovie(addedMovie, moneyTo);

		// when
		movieService.deleteMovieById(updatedMovie.getId());

		// then
		assertTrue(movieService.countMovies() == 0);
		assertTrue(movieService.countMoneys() == 0);
	}
}
