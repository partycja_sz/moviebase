package com.jstk.movie.service.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jstk.movie.criteria.MovieSearchCriteria;
import com.jstk.movie.dao.MovieRepository;
import com.jstk.movie.domain.MovieEntity;
import com.jstk.movie.types.MovieTO;


@Component
public class MovieServiceValidator {

	@Autowired
	private MovieRepository movieRepository;
	
	public boolean validateMovieExistence(MovieTO movieTO) {
		MovieSearchCriteria movieSearchCriteria = new MovieSearchCriteria();
		movieSearchCriteria.setCountry(movieTO.getCountry());
		movieSearchCriteria.setReleaseYear(movieTO.getReleaseDate());
		movieSearchCriteria.setTitle(movieTO.getTitle());
		List<MovieEntity> movieList = movieRepository.findAllMoviesByCriteria(movieSearchCriteria);
		return (!movieList.isEmpty());
	}
}
