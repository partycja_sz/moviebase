package com.jstk.movie.types;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.jstk.movie.enums.MovieCategory;
import com.jstk.movie.enums.MovieType;

public class MovieTO {
	private Long id;
	private String title;
	private MovieCategory movieCategory;
	private MovieType movieType;
	private int runtime;
	private LocalDate releaseDate;
	private String country;
	private List<CastTO> cast;
	private StudioTO studio;
	private Boolean threeD;
	private BigDecimal budget;
	private BigDecimal profitGeneral;
	private BigDecimal oneWeekProfit;
	private Long version;
	private LocalDateTime  creationDate;
	private LocalDateTime  modyficationDate;
	private List<MoneyTO> money = new ArrayList<>();

	public MovieTO(Long id, String title, MovieCategory movieCategory, MovieType movieType, int runtime,
			LocalDate releaseDate, String country, List<CastTO> cast, StudioTO studio, Boolean threeD, BigDecimal budget,
			BigDecimal profitGeneral, BigDecimal oneWeekProfit, List<MoneyTO> money) {
		super();
		this.id = id;
		this.title = title;
		this.movieCategory = movieCategory;
		this.movieType = movieType;
		this.runtime = runtime;
		this.releaseDate = releaseDate;
		this.country = country;
		this.cast = cast;
		this.studio = studio;
		this.threeD = threeD;
		this.budget = budget;
		this.profitGeneral = profitGeneral;
		this.oneWeekProfit = oneWeekProfit;
		this.money = money;
	}
	
	public MovieTO(Long id, String title, MovieCategory movieCategory, MovieType movieType, int runtime,
			LocalDate releaseDate, String country, List<CastTO> cast, StudioTO studio, Boolean threeD, BigDecimal budget,
			BigDecimal profitGeneral, BigDecimal oneWeekProfit, Long version, LocalDateTime  creationDate,
			LocalDateTime  modyficationDate, List<MoneyTO> money) {
		super();
		this.id = id;
		this.title = title;
		this.movieCategory = movieCategory;
		this.movieType = movieType;
		this.runtime = runtime;
		this.releaseDate = releaseDate;
		this.country = country;
		this.cast = cast;
		this.studio = studio;
		this.threeD = threeD;
		this.budget = budget;
		this.profitGeneral = profitGeneral;
		this.oneWeekProfit = oneWeekProfit;
		this.version = version;
		this.creationDate = creationDate;
		this.modyficationDate = modyficationDate;
		this.money = money;
	}

	public MovieTO(){}

	public static MovieTOBuilder builder() {
		return new MovieTOBuilder();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public MovieCategory getMovieCategory() {
		return movieCategory;
	}

	public void setMovieCategory(MovieCategory movieCategory) {
		this.movieCategory = movieCategory;
	}

	public MovieType getMovieType() {
		return movieType;
	}

	public void setMovieType(MovieType movieType) {
		this.movieType = movieType;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public LocalDate getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<CastTO> getCast() {
		return cast;
	}

	public void setCast(List<CastTO> cast) {
		this.cast = cast;
	}

	public StudioTO getStudio() {
		return studio;
	}

	public void setStudio(StudioTO studio) {
		this.studio = studio;
	}

	public Boolean getThreeD() {
		return threeD;
	}

	public void setThreeD(Boolean threeD) {
		this.threeD = threeD;
	}

	public BigDecimal getBudget() {
		return budget;
	}

	public void setBudget(BigDecimal budget) {
		this.budget = budget;
	}

	public BigDecimal getProfitGeneral() {
		return profitGeneral;
	}

	public void setProfitGeneral(BigDecimal profitGeneral) {
		this.profitGeneral = profitGeneral;
	}

	public BigDecimal getOneWeekProfit() {
		return oneWeekProfit;
	}

	public void setOneWeekProfit(BigDecimal oneWeekProfit) {
		this.oneWeekProfit = oneWeekProfit;
	}

	public Long getVersion() {
		return version;
	}

	public LocalDateTime  getCreationDate() {
		return creationDate;
	}

	public LocalDateTime  getModyficationDate() {
		return modyficationDate;
	}
	
	public void setVersion(Long version) {
		this.version = version;
	}

	public void setCreationDate(LocalDateTime  creationDate) {
		this.creationDate = creationDate;
	}

	public void setModyficationDate(LocalDateTime  modyficationDate) {
		this.modyficationDate = modyficationDate;
	}
	
	public List<MoneyTO> getMoney() {
		return money;
	}

	public void setMoney(List<MoneyTO> money) {
		this.money = money;
	}

	public static class MovieTOBuilder {
		private Long id;
		private String title;
		private MovieCategory movieCategory;
		private MovieType movieType;
		private int runtime;
		private LocalDate releaseDate;
		private String country;
		private List<CastTO> cast;
		private StudioTO studio;
		private Boolean threeD;
		private BigDecimal budget;
		private BigDecimal profitGeneral;
		private BigDecimal oneWeekProfit;
		private List<MoneyTO> money = new ArrayList<>();

		public MovieTOBuilder() {
			super();
		}

		public MovieTOBuilder withId(Long id) {
			this.id = id;
			return this;
		}

		public MovieTOBuilder withTitle(String title) {
			this.title = title;
			return this;
		}

		public MovieTOBuilder withMovieCategory(MovieCategory movieCategory) {
			this.movieCategory = movieCategory;
			return this;
		}

		public MovieTOBuilder withMovieType(MovieType movieType) {
			this.movieType = movieType;
			return this;
		}

		public MovieTOBuilder withReleaseDate(LocalDate releaseDate) {
			this.releaseDate = releaseDate;
			return this;
		}

		public MovieTOBuilder withRuntime(int runtime) {
			this.runtime = runtime;
			return this;
		}
		
		public MovieTOBuilder withCountry(String country) {
			this.country = country;
			return this;
		}
		
		public MovieTOBuilder withStudio(StudioTO studio) {
			this.studio = studio;
			return this;
		}
		
		public MovieTOBuilder withThreeD(Boolean threeD) {
			this.threeD = threeD;
			return this;
		}
		
		public MovieTOBuilder withBudget(BigDecimal budget) {
			this.budget = budget;
			return this;
		}
		
		public MovieTOBuilder withProfitGeneral(BigDecimal profitGeneral) {
			this.profitGeneral = profitGeneral;
			return this;
		}
		
		public MovieTOBuilder withOneWeekProfit(BigDecimal oneWeekProfit) {
			this.oneWeekProfit = oneWeekProfit;
			return this;
		}
		
		public MovieTOBuilder withCast(List<CastTO> cast) {
			this.cast = cast;
			return this;
		}
		
		public MovieTOBuilder withMoney(List<MoneyTO> money) {
			this.money = money;
			return this;
		}
		
		public MovieTO build() {
			checkBeforeBuild(title,  movieCategory,  movieType,  runtime,
					 releaseDate,  country,  cast,  studio,  threeD,  budget,
					 profitGeneral,  oneWeekProfit, money);
			return new MovieTO(id,  title,  movieCategory,  movieType,  runtime,
					 releaseDate,  country,  cast,  studio,  threeD,  budget,
					 profitGeneral,  oneWeekProfit, money);
		}

		private void checkBeforeBuild(String title, MovieCategory movieCategory, MovieType movieType, int runtime,
				LocalDate releaseDate, String country, List<CastTO> cast, StudioTO studio, Boolean threeD, BigDecimal budget,
				BigDecimal profitGeneral, BigDecimal oneWeekProfit, List<MoneyTO> money) {
			if (title == null || title.isEmpty() ) {
				throw new IllegalArgumentException("Incorrect movie to be created");
			}
		}
	}
}
