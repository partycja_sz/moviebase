package com.jstk.movie.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.jstk.movie.custom.dao.MovieRepositoryCustom;
import com.jstk.movie.domain.MovieEntity;

public interface MovieRepository extends JpaRepository<MovieEntity, Long>, MovieRepositoryCustom {

}
