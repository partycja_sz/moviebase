package com.jstk.movie.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class StudioEntity extends AbstractEntity {

	@Column(length = 100, nullable = false)
	private String name;
	@Column(length = 100, nullable = false)
	private String address;

	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY, mappedBy = "studio")
	private List<MovieEntity> movies = new ArrayList<>();

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinTable(name = "studio_cast", joinColumns = { @JoinColumn(name = "studio_id") }, inverseJoinColumns = {
			@JoinColumn(name = "cast_id") })
	private List<CastEntity> casts = new ArrayList<>();

	public StudioEntity() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<MovieEntity> getMovies() {
		return movies;
	}

	public void setMovies(List<MovieEntity> movies) {
		this.movies = movies;
	}

	public List<CastEntity> getCasts() {
		return casts;
	}

	public void setCasts(List<CastEntity> casts) {
		this.casts = casts;
	}

}
