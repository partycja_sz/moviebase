package com.jstk.movie;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.jstk.movie.enums.MovieCategory;
import com.jstk.movie.enums.MovieType;
import com.jstk.movie.exceptions.DuplicatedMovieHasBeenFoundException;
import com.jstk.movie.service.MovieService;
import com.jstk.movie.service.StudioService;
import com.jstk.movie.types.MovieTO;
import com.jstk.movie.types.StudioTO;
import com.jstk.movie.types.MovieTO.MovieTOBuilder;
import com.jstk.movie.types.StudioTO.StudioTOBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudioServiceTests {

	@Autowired
	private StudioService studioService;

	@Autowired
	private MovieService movieService;
	
	DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Test
	public void addStudioTest() {
		// given
		String name = "Digital";
		String address = "Cinema Street 12345, Los Angeles, USA";

		// when
		StudioTO studioTo = new StudioTOBuilder().withName(name).withAddress(address).build();
		StudioTO addedStudio = studioService.addStudio(studioTo);

		// then
		assertEquals(name, addedStudio.getName());
		assertEquals(address, addedStudio.getAddress());
		assertNotNull(addedStudio.getCreationDate());
		assertTrue(addedStudio.getVersion().equals(0L));
	}

	@Test
	public void updateStudioTest() {
		// given
		String name = "Digital";
		String address = "Cinema Street 12345, Los Angeles, USA";
		String addressUpdated = "New Street great 453/23, Los Angeles, USA";

		StudioTO studioTo = new StudioTOBuilder().withName(name).withAddress(address).build();
		StudioTO addedStudio = studioService.addStudio(studioTo);
		Long id = addedStudio.getId();

		// when
		addedStudio.setAddress(addressUpdated);
		StudioTO updatedStudio = studioService.updateStudio(addedStudio);

		// then
		assertEquals(name, updatedStudio.getName());
		assertEquals(addressUpdated, updatedStudio.getAddress());
		assertNotNull(updatedStudio.getCreationDate());
		assertTrue(updatedStudio.getModyficationDate().compareTo(updatedStudio.getCreationDate()) >= 0);
		assertTrue(updatedStudio.getVersion().equals(1L));
		assertTrue(updatedStudio.getId().equals(id));
	}

	@Test
	public void findStudioByIdTest() {
		// given
		String name = "Digital";
		String address = "Cinema Street 12345, Los Angeles, USA";
		StudioTO studioTo = new StudioTOBuilder().withName(name).withAddress(address).build();
		StudioTO addedStudio = studioService.addStudio(studioTo);

		// when
		StudioTO foundStudio = studioService.findStudioById(addedStudio.getId());

		// then
		assertEquals(name, foundStudio.getName());
		assertEquals(address, foundStudio.getAddress());
		assertNotNull(foundStudio.getCreationDate());
		assertTrue(foundStudio.getVersion().equals(0L));
	}
	
	@Test
	public void deleteStudioByIdTest() {
		// given
		String name = "Digital";
		String address = "Cinema Street 12345, Los Angeles, USA";
		StudioTO studioTo = new StudioTOBuilder().withName(name).withAddress(address).build();
		StudioTO addedStudio = studioService.addStudio(studioTo);

		// when
		studioService.removeStudio(addedStudio);

		// then
		assertTrue(studioService.countAllStudios()==0L);
	}
	
	@Test
	public void addMovieToStudioTest() throws DuplicatedMovieHasBeenFoundException {
		// given
		String name = "Digital";
		String address = "Cinema Street 12345, Los Angeles, USA";
		StudioTO studioTo = new StudioTOBuilder().withName(name).withAddress(address).build();
		StudioTO addedStudio = studioService.addStudio(studioTo);
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		MovieTO movieTo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();
		MovieTO addedMovie = movieService.addMovie(movieTo);
		
		// when
		studioService.addMovieToStudio(addedStudio,addedMovie);
		
		// then
		assertFalse(addedStudio.getMovies().isEmpty());
		assertEquals(name ,addedMovie.getStudio().getName());
	}
	
	@Test
	public void deleteStudioWithMoviesTest() throws DuplicatedMovieHasBeenFoundException {
		// given
		String name = "Digital";
		String address = "Cinema Street 12345, Los Angeles, USA";
		StudioTO studioTo = new StudioTOBuilder().withName(name).withAddress(address).build();
		StudioTO addedStudio = studioService.addStudio(studioTo);
		String dateRelaseString = "2003-05-15";
		int runtime = 190;
		String title = "Gladiator";
		MovieCategory movieCategory = MovieCategory.THRILLER;
		MovieType movieType = MovieType.COLOR;
		LocalDate releaseDate = LocalDate.parse(dateRelaseString, dateFormatter);
		String country = "USA";
		Boolean threeD = false;
		BigDecimal budget = new BigDecimal("123456.89");
		BigDecimal profitGeneral = new BigDecimal("521000.12");
		BigDecimal oneWeekProfit = new BigDecimal("94512.01");

		MovieTO movieTo = new MovieTOBuilder().withTitle(title).withThreeD(threeD).withRuntime(runtime)
				.withMovieCategory(movieCategory).withMovieType(movieType).withCountry(country).withBudget(budget)
				.withReleaseDate(releaseDate).withOneWeekProfit(oneWeekProfit).withProfitGeneral(profitGeneral).build();
		MovieTO addedMovie = movieService.addMovie(movieTo);
		studioService.addMovieToStudio(addedStudio,addedMovie);
		
		// when
		StudioTO studio = studioService.findStudioById(addedStudio.getId());
		studioService.removeStudio(studio);
		
		// then
		assertTrue(studioService.countAllStudios()==0L);
		assertTrue(movieService.countMovies()==1L);
	}
}
