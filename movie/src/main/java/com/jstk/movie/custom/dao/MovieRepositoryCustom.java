package com.jstk.movie.custom.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.jstk.movie.criteria.MovieSearchCriteria;
import com.jstk.movie.domain.MovieEntity;
import com.jstk.movie.domain.StudioEntity;

public interface MovieRepositoryCustom  {
	
	List<BigDecimal> findMoviesBudgetBetweenSpecifiedDates(LocalDate dateFrom, LocalDate dateTo);

	List<MovieEntity> findAllMoviesByCriteria(MovieSearchCriteria movieSearchCriteria);
	
	List<MovieEntity> findTheLongestMovieInSpecifiedStudioInSpecifiedTime(LocalDate dateFrom, LocalDate dateTo, StudioEntity studioEntity);

	Map<MovieEntity, BigDecimal> findWholeEarnMoneyForSpecifiedNoOfMovies(int noOfTopExpensiveMovies);

}
