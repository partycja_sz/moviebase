package com.jstk.movie.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jstk.movie.domain.MoneyEntity;

@Repository
public interface MoneyRepository extends JpaRepository<MoneyEntity, Long>{

}
