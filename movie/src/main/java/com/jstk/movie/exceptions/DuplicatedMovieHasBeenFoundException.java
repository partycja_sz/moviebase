package com.jstk.movie.exceptions;

public class DuplicatedMovieHasBeenFoundException extends Exception {
	public DuplicatedMovieHasBeenFoundException() {
		super("This movie already exists in database!");
	}
}
